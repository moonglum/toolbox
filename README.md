# toolbox

An experimental replacement for my current devbox setup.

## Setup

* `buildah build -t toolbox`
* `podman run -it --rm -v=./tilde:/home/moonglum:U toolbox`
