FROM docker.io/library/alpine:edge

RUN apk add --no-cache\
  eza=0.18.10-r0\
  fd=9.0.0-r0\
  fish=3.7.1-r0\
  fzf=0.49.0-r1\
  git=2.44.0-r2\
  neofetch=7.1.0-r1\
  nodejs=20.12.2-r0\
  ripgrep=14.1.0-r0\
  ruby=3.3.0-r0\
  tmux=3.4-r1\
  vim=9.1.0-r2

RUN addgroup -S -g 1000 moonglum &&\
  adduser -S -G moonglum -u 1000 -s /usr/bin/fish moonglum

USER moonglum
CMD ["/usr/bin/fish"]
